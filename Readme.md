DESCRIPTION:

Бекенд на Rust с использованием фреймворка Rocket. Поднимает сервер с сайтом по адрессу 127.0.0.1:8080

Backend on Rust using the Rocket framework. Starts the server with the site on 127.0.0.1:8080

###############################

REQUIREMENTS:
    
    cargo

###############################

COMPILATION:

    buildn:
      make 

    run:
      ./main 

###############################
